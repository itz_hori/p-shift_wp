<?php
/**
 * テーマ内で汎用的に使われるコード
 *
 * @package    WordPress
 */

/**---------------------------------------------
 * CSS・JSファイルを読み込み
 * -------------------------------------------*/
function add_my_files() {
	/* CSS */
	wp_enqueue_style(
		'my-style',
		get_theme_file_uri( '/assets/css/style.css' ),
		array(),
		filemtime( get_theme_file_path( '/assets/css/style.css' ) ) /* キャッシュ対策 */
	);
	if ( ! is_admin() ) {
		/* WordPress 本体の jQuery を登録解除 */
		wp_deregister_script( 'jquery' );
		/* jQuery CDN */
		wp_enqueue_script(
			'jquery',
			'//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js',
			array(),
			'3.5.1',
			false
		);
	}

	/* JS */
	wp_enqueue_script(
		'my-script',
		get_theme_file_uri( '/assets/js/bundle.js' ),
		array( 'jquery' ), /* 依存関係を設定 */
		filemtime( get_theme_file_path( '/assets/js/bundle.js' ) ), /* キャッシュ対策 */
		true
	);
}
add_action( 'wp_enqueue_scripts', 'add_my_files' );


/**---------------------------------------------
 * JSにdefer属性を付ける
 * -------------------------------------------*/
add_filter(
	'script_loader_tag',
	function ( $tag, $handle ) {
		/* 特定の名前のものに付ける場合 */
		if ( 0 === strpos( $handle, 'my-script' ) ) {
			$tag = str_replace( ' src', ' defer src', $tag );
		}
		return $tag;
	},
	10,
	2
);


/**---------------------------------------------
 * 機能の呼び出し
 * -------------------------------------------*/
function my_setup() {
	add_theme_support( 'post-thumbnails' ); /* アイキャッチ設定を有効化! */
	add_theme_support( 'html5', array( 'search-form' ) ); /* 検索機能を有効化 */
}
add_action( 'after_setup_theme', 'my_setup' );


/**---------------------------------------------
 * Gutenberg(編集画面)にCSSを適用
 * -------------------------------------------*/
add_action(
	'enqueue_block_editor_assets',
	function() {
		$editor_style_url = get_theme_file_uri( '/editor-style.css', );
		wp_enqueue_style( 'theme-editor-style', $editor_style_url, array( 'wp-block-library' ), true );
	}
);

/**---------------------------------------------
 * お知らせ（ページ更新情報）をカスタム投稿として作成
 * -------------------------------------------*/
function add_custom_post() {
	/** カスタム投稿タイプの設定 */
	register_post_type(
		'info',
		array(
			'label'         => 'お知らせ', /* 管理画面状の表示名 */
			'public'        => true,
			'has_archive'   => true, /* アーカイブを有効にする */
			'hierarchical'  => false, /* ページ階層の指定 */
			'menu_position' => 5, /* 管理画面上の配置指定 */
			'menu_icon'     => 'dashicons-info',
			'show_in_rest'  => true, /* Gutenbergを使用 */
			'rewrite'       => array( 'with_front' => false ), /* 投稿タイプのパーマリンクのリライト方法を変更 */
			'supports'      => array( /* 編集画面で使用できるようにする機能を指定 */
				'title',
				'editor',
				'thumbnail',
				'revisions',
				'excerpt',
			),
		)
	);
	/** カスタムタクソノミーの設定 */
	register_taxonomy(
		'info-cat', /* タクソノミースラッグ  */
		'info',     /* 使用するカスタム投稿タイプ */
		array(
			'hierarchical'   => true, /* trueならカテゴリ、falseならタグ */
			'label'          => 'お知らせカテゴリー',
			'singular_label' => 'お知らせカテゴリー',
			'labels'         => array(
				'all_items'    => 'お知らせカテゴリ一覧',
				'add_new_item' => 'お知らせカテゴリを追加',
			),
			'public'         => true,
			'show_ui'        => true,
		)
	);
}
add_action( 'init', 'add_custom_post' );


/**---------------------------------------------
 * カスタム投稿のパーマリンクを数字ベース（投稿ID）に
 *
 * @param string $link 引数の説明.
 * @param string $post 引数の説明.
 */
function info_post_type_link( $link, $post ) {
	if ( 'info' === $post->post_type ) {
		return home_url( '/info/' . $post->ID );
	} else {
		return $link;
	}
}
add_filter( 'post_type_link', 'info_post_type_link', 1, 2 );

/**
 * URLを書き換え
 *
 * @param string $rules 引数.
 */
function info_rewrite_rules_array( $rules ) {
	$new_rewrite_rules = array(
		'info/([0-9]+)/?$' => 'index.php?post_type=info&p=$matches[1]',
	);
	return $new_rewrite_rules + $rules;
}
add_filter( 'rewrite_rules_array', 'info_rewrite_rules_array' );


/**---------------------------------------------
 * アップロードされた画像ファイル名をランダム文字にする
 *
 * @param string $file_name アップロードされた時点の画像ファイル名.
 */
function rename_upload_image_file( $file_name ) {
	/* ファイル名が大文字の場合もあるので、ファイル名全てを小文字に変換する */
	$file_name = strtolower( $file_name );
	/* 「.」の位置を取得 */
	$index = strrpos( $file_name, '.' );
	/* 拡張子を取得 */
	$exts = $index ? '.' . substr( $file_name, $index + 1 ) : '';

	/* 画像ファイルか拡張子でチェックする */
	if ( in_array( $exts, array( '.jpg', '.jpeg', '.gif', '.png' ), true ) ) {
		/* ファイル名をmd5で生成する */
		$file_name = md5( time() . $file_name ) . $exts;
	}

	return $file_name;
}
add_filter( 'sanitize_file_name', 'rename_upload_image_file', 10 );


/**---------------------------------------------
 * lazyblockで自動で付与されるdivを削除
 * -------------------------------------------*/
add_filter( 'lazyblock/media-text-row/frontend_allow_wrapper', '__return_false' );
add_filter( 'lazyblock/media-text-column/frontend_allow_wrapper', '__return_false' );
