<?php
/**
 * 検索結果表示テンプレート
 *
 * @package    WordPress
 */

?>

<?php get_header(); ?>

<!-- section -->
<section class="l-section">
	<div class="l-container">
		<div class="c-grid">

			<!-- メイン -->
			<div class="c-grid__item -main">
				<div class="p-panel -large">
					<h2 class="c-heading -primary -icon">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icon-search.svg" alt="" width="32" height="32"><span>「<?php the_search_query(); ?> 」の検索結果</span>
					</h2>
					<div class="p-panel__body">
						<?php
						$search_posts = $wp_query->found_posts;
						if ( have_posts() && get_search_query() ) :
							echo '<p class="p-panel__search-text">' . esc_html( $search_posts ) . '件の記事が見つかりました。</p>';
							?>

							<ul class="p-panel__list">
							<?php
							while ( have_posts() ) :
								the_post();
								?>

								<?php get_template_part( 'template-parts/panel-item' ); ?>

							<?php endwhile; ?>
							</ul>

						<?php else : ?>

							<p>記事が見つかりませんでした。</p>
							<div class="p-error-search">
								<div class="p-error-search__button">
									<a class="c-button -primary -contact" href="<?php echo esc_url( home_url() ); ?>">
										<svg class="u-svg-home">
											<use xlink:href="#svg-icon-home"></use>
										</svg><span>ホームへ戻る</span>
									</a>
								</div>
								<div class="p-error-search__field">
									<?php get_search_form(); ?>
								</div>
							</div>

						<?php endif; ?>
					</div>
				</div><!-- /.p-panel -->
			</div><!-- /.c-grid__item.-main -->

			<!-- サイドバー -->
			<?php get_sidebar(); ?>

		</div><!-- ./c-grid -->
	</div><!-- .l-container -->
</section>

<?php get_footer(); ?>
