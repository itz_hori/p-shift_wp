<?php
/**
 * カテゴリーページのテンプレート
 *
 * @package    WordPress
 */

?>

<?php get_header(); ?>

<!-- section -->
<section class="l-section">
	<div class="l-container">
		<div class="c-grid">
			<!-- メイン -->
			<div class="c-grid__item -main">
				<div class="p-panel -large">
					<h2 class="c-heading -primary -icon">
					<?php
					/* カテゴリ毎に画像パスを書き換えて出力 */
					$category = get_the_category();
					$category = $category[0];
					echo '<img src="' . esc_url( get_template_directory_uri() ) . '/assets/img/icon-' . esc_html( $category->slug ) . '.svg" alt="' . esc_html( $category->name ) . 'アイコン"><span>' . esc_html( $category->name ) . '</span>';
					?>
					</h2>
					<div class="p-panel__body -mg-small">
						<ul class="p-panel__list">
						<?php
						if ( have_posts() ) :
							while ( have_posts() ) :
								the_post();
								?>

								<?php get_template_part( 'template-parts/panel-item' ); ?>

									<?php
								endwhile;
							else :
								?>

							<p class="u-mt-24">
								記事が見つかりませんでした。
							</p>
							<div class="p-error-search">
								<div class="p-error-search__button">
									<a class="c-button -primary -contact" href="<?php echo esc_url( home_url() ); ?>">
										<svg class="u-svg-home">
											<use xlink:href="#svg-icon-home"></use>
										</svg><span>ホームへ戻る</span>
									</a>
								</div>
								<div class="p-error-search__field">
									<form class="p-search" role="search">
										<input type="search" placeholder="キーワードを入力して検索" class="c-input -bg-dark p-search__field">
										<button type="submit" class="c-button p-search__submit">
											<svg>
												<use xlink:href="#svg-icon-search"></use>
											</svg>
										</button>
									</form>
								</div>
							</div>

								<?php
						endif;
							?>
						</ul>
					</div>
				</div><!-- /.p-panel -->
			</div><!-- /.c-grid__item.-main -->

			<?php get_sidebar(); ?>

		</div><!-- ./c-grid -->
	</div><!-- .l-container -->
</section>

<?php get_footer(); ?>
