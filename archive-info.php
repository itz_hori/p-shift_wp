<?php
/**
 * お知らせ記一覧テンプレート
 * 「お知らせ」はカスタム投稿として管理
 *
 * @package    WordPress
 */

?>

<?php get_header(); ?>

<!-- section -->
<section class="l-section">
	<div class="l-container">
		<div class="c-grid">
		<?php
		$category_info = get_post_type_object( 'info' );
		?>
			<!-- メイン -->
			<div class="c-grid__item -main">
				<div class="p-panel -large">
					<h2 class="c-heading -primary -icon">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icon-<?php echo esc_html( $category_info->name ); ?>.svg" alt="" width="32" height="32"><span><?php echo esc_html( $category_info->label ); ?></span>
					</h2>
					<div class="p-panel__body -mg-small">
						<ul class="p-panel__list">
						<?php
						if ( have_posts() ) :
							while ( have_posts() ) :
								the_post();
								?>

								<?php
									set_query_var( 'location', 'info' );
									get_template_part( 'template-parts/panel-item' );
								?>

									<?php
								endwhile;
							else :
								?>

							<p class="u-mt-24">
								記事が見つかりませんでした。
							</p>
							<div class="p-error-search">
								<div class="p-error-search__button">
									<a class="c-button -primary -contact" href="<?php echo esc_url( home_url() ); ?>">
										<svg class="u-svg-home">
											<use xlink:href="#svg-icon-home"></use>
										</svg><span>ホームへ戻る</span>
									</a>
								</div>
								<div class="p-error-search__field">
									<form class="p-search" role="search">
										<input type="search" placeholder="キーワードを入力して検索" class="c-input -bg-dark p-search__field">
										<button type="submit" class="c-button p-search__submit">
											<svg>
												<use xlink:href="#svg-icon-search"></use>
											</svg>
										</button>
									</form>
								</div>
							</div>

								<?php
						endif;
							?>
						</ul>
					</div>
				</div><!-- /.p-panel -->
			</div><!-- /.c-grid__item.-main -->

			<?php get_sidebar(); ?>

		</div><!-- ./c-grid -->
	</div><!-- .l-container -->
</section>

<?php get_footer(); ?>
