/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _scss_style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../scss/style.scss */ \"./src/scss/style.scss\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ \"jquery\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);\n/*\nImport\n-----------------------------------------------------*/\n // scssの読み込み\n\n // jQueryの読み込み\n\n/*\nここからjQuery\n-----------------------------------------------------*/\n\nvar $ = jquery__WEBPACK_IMPORTED_MODULE_1___default.a; // ドルマークに参照を代入(慣習的な $ を使うß\n\njquery__WEBPACK_IMPORTED_MODULE_1___default()(function () {\n  pageTop();\n  /* IEエラー防止のため、要素が存在する場合のみ関数を実行 */\n\n  if ($('.js-home-panel').length) {\n    checkArticles();\n  }\n\n  if ($('.js-side-accordion').length) {\n    accordionMenu();\n    openAccordion();\n  }\n\n  if ($('.js-popup-trigger').length) {\n    popUp();\n  }\n});\n/* 記事数をチェックする\n--------------------------*/\n\nvar checkArticles = function checkArticles() {\n  var categories = $('.js-home-panel');\n\n  for (var i = 0; i < categories.length; i++) {\n    var category = categories[i]; // 各カテゴリの親要素をelementに代入\n    //console.log(category);\n    // 各カテゴリの記事数をカウント\n\n    var number = $(category).find('.p-panel__item').length; // 記事数＝0の場合は、ーdisabledクラスを付与\n\n    if (number === 0) {\n      $(category).addClass('-disabled');\n    }\n  }\n};\n/* アコーディオン\n--------------------------*/\n\n\nvar accordionMenu = function accordionMenu() {\n  var $trigger = $('.js-side-accordion-trigger'); // button要素トリガー\n\n  $trigger.on('click', function () {\n    // ボタンの隣接要素を取得\n    var $accordion = $(this).next('.p-side-list__accordion'); // アコーディオンの高さを取得(直下の子要素のul)\n\n    var accordionHeight = $accordion.children('ul').innerHeight(); // アコーディオンを閉じるCSSを定義\n\n    var props = {\n      'height': '0',\n      'visibility': 'hidden',\n      'transition-duration': '.3s'\n    }; // button要素が is-open クラスを持っているかどうかで判定\n\n    if ($(this).hasClass('is-open')) {\n      $(this).removeClass('is-open');\n      $accordion.css(props);\n    } else {\n      // アコーディオンを複数開かせない（button要素とアコーディオンをデフォルトに戻す）\n      $('.js-side-accordion-trigger').removeClass('is-open');\n      $('.p-side-list__accordion').css(props);\n      $(this).addClass('is-open');\n      $accordion.css({\n        height: accordionHeight + 'px',\n        visibility: 'visible'\n      });\n    }\n  });\n};\n/* カレントページのURLが存在するとき\nアコーディオンメニューを開く\n--------------------------*/\n\n\nvar openAccordion = function openAccordion() {\n  var currentUrl = location.href; // 現在のURL\n\n  var anchors = $('.js-side-accordion > ul > li > a');\n\n  for (var i = 0; i < anchors.length; i++) {\n    var anchorsUrl = $(anchors[i]).attr('href'); // aタグのhref属性を取得\n\n    if (anchorsUrl === currentUrl) {\n      // aタグにis-currentクラスを付与\n      $(anchors[i]).addClass('is-current'); // 子要素にis-currentクラスを持つ親要素アコーディオンを取得\n\n      var $accordion = $(anchors[i]).parents('.p-side-list__accordion'); // アコーディオンの高さを取得(直下の子要素のul)\n\n      var accordionHeight = $accordion.children('ul').innerHeight(); // アコーディオンを開く\n\n      $accordion.css({\n        height: accordionHeight + 'px',\n        visibility: 'visible'\n      }); // 前にある兄弟要素buttonにis-openクラスを付与\n\n      $accordion.prev('.p-side-list__button').addClass('is-open');\n    }\n  }\n};\n/* ポップアップ\n--------------------------*/\n\n\nvar popUp = function popUp() {\n  // マウスカーソルが乗った時\n  $('.js-popup-trigger').on('mouseenter', function () {\n    $(this).children('a').addClass('is-active');\n  }); // マウスカーソルが離れた時\n\n  $('.js-popup-trigger').on('mouseleave', function () {\n    $(this).children('a').removeClass('is-active');\n  });\n};\n/* スムーススクロール(TOP)\n--------------------------*/\n\n\nvar pageTop = function pageTop() {\n  var $target = $('#js-page-top > a');\n  $target.on('click', function () {\n    $('html, body').animate({\n      scrollTop: 0\n    }, '400', 'swing');\n    return false;\n  });\n};\n\n//# sourceURL=webpack:///./src/js/app.js?");

/***/ }),

/***/ "./src/scss/style.scss":
/*!*****************************!*\
  !*** ./src/scss/style.scss ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack:///./src/scss/style.scss?");

/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = jQuery;\n\n//# sourceURL=webpack:///external_%22jQuery%22?");

/***/ })

/******/ });