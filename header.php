<?php
/**
 * Headerテンプレート
 *
 * @package    WordPress
 */

?>

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo esc_html( wp_get_document_title() ); ?></title>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> id="top" >
	<!-- svg -->
	<?php get_template_part( './template-parts/svg' ); /** SVG定義ファイルの読み込み */ ?>

	<!-- header -->
	<header class="l-header">
		<div class="l-container">
			<div class="l-header__body">
				<div class="l-header__logo">
					<a href="<?php echo esc_url( home_url() ); ?>">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/logo.svg" alt="P-Shiftガイド">
					</a>
				</div>
			</div>
		</div>
	</header>

	<!-- main -->
	<main class="l-main">

	<?php if ( ! ( is_home() || is_front_page() ) ) : ?>
	<!-- パンくず -->
	<nav class="p-breadcrumb">
		<div class="l-container">
			<ol class="p-breadcrumb__list">
			<?php
			if ( function_exists( 'bcn_display' ) ) {
				bcn_display_list();
			}
			?>
			</ol>
		</div>
	</nav>
	<?php endif; ?>

		<!-- hero -->
		<?php if ( is_front_page() ) : /* TOPページのとき */ ?>

		<section class="p-hero -home">
			<div class="l-container">
				<div class="p-hero__inner">
					<!-- primary -->
					<div class="p-hero__secondary">
						<h1 class="p-hero__subtitle">クラウド勤怠管理システムP-Shiftサポートガイド</h1>
						<p class="c-heading p-hero__title">どんなことにお困りですか？</p>
						<!-- 検索フォーム -->
						<div class="p-hero__search-module">
							<?php get_search_form(); /* フォーム（searchform.php）を呼び出し */ ?>
						</div>
						<!-- 検索キーワード -->
						<div class="p-hero__keyword">
							<?php
							$args = array(
								'number' => 5, /* 最大5件取得 */
							);
							get_template_part( './template-parts/tag', null, $args );
							?>
						</div>
					</div>
					<!-- secondary -->
					<div class="p-hero__primary">
						<figure class="p-hero__image">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/hero-top.png" alt="クラウド勤怠管理システムP-Shiftタブレットと管理画面">
						</figure>
					</div>
				</div>
			</div>
		</section>

		<?php else : /* それ以外のとき*/ ?>
		<section class="p-hero">
			<div class="l-container">
				<div class="p-hero__inner">
					<!-- primary -->
					<div class="p-hero__primary">
						<p class="p-hero__subtitle">P-Shiftサポートガイド</p>
						<?php
						if ( is_post_type_archive( 'info' ) || is_singular( 'info' ) ) {
							/* カスタム投稿「info」のとき */
							$page_title = 'お知らせ';
						} elseif ( is_category() || is_tag() ) {
							/* 各カテゴリ名を取得（第二引数にfalseを指定で変数に代入） */
							$page_title = single_cat_title( '', false );
						} elseif ( is_single() ) {
							$page_title = get_the_title();
						} elseif ( is_search() ) {
							$page_title = '検索結果';
						} elseif ( is_404() ) {
							$page_title = '404 Not Found';
						}
						?>
						<h1 class="c-heading p-hero__title"><?php echo esc_html( $page_title ); ?></h1>
					</div>
					<!-- secondary -->
					<div class="p-hero__secondary">
						<!-- 検索フォーム -->
						<div class="p-hero__search-module">
							<?php get_search_form(); /* フォーム（searchform.php）を呼び出し */ ?>
						</div>
						<!-- 検索キーワード -->
						<div class="p-hero__keyword">
							<?php
							$args = array(
								'number' => 5, /* 最大5件取得 */
							);
							get_template_part( './template-parts/tag', null, $args );
							?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php endif; ?>
