<?php
/**
 * Footerテンプレート
 *
 * @package    WordPress
 */

?>

<!-- footer -->
<footer class="l-footer">
	<!-- ページトップボタン -->
	<div class="p-page-top" id="js-page-top">
		<a href="#top">
			<svg>
				<use xlink:href="#svg-page-top"></use>
			</svg>
		</a>
	</div>
	<div class="l-container">
		<div class="l-footer__inner">
			<!-- ロゴ・コピーライト -->
			<div class="l-footer__address">
				<p class="l-footer__logo">
					<a href="<?php echo esc_url( home_url() ); ?>">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/logo-reverse.svg" alt="P-Shiftガイド">
					</a>
				</p>
				<p class="l-footer__copyright"><small>2020©ITZmkt inc.</small></p>
			</div>
			<!-- フッターメニュー -->
			<div class="l-footer__nav p-nav">
				<!-- カテゴリー -->
				<div class="p-nav__block">
					<p class="p-nav__title">カテゴリー</p>
					<ul class="p-nav__menu">

					<?php
					$categories = get_categories(); /** カテゴリーを取得（記事数が0以上のみ） */
					foreach ( $categories as $category ) {
						echo '<li class="p-nav__item"><a href="' . esc_url( home_url( $category->slug ) ) . '/">' . esc_html( $category->cat_name ) . '</a></li>';
					}
					?>

					</ul>
				</div>
				<!-- キーワード -->
				<div class="p-nav__block">
					<p class="p-nav__title">キーワード</p>
					<!-- 検索キーワード -->
					<div class="p-nav__menu">
						<?php
						$args = array(
							'number' => '',
						);
						get_template_part( './template-parts/tag', null, $args );
						?>
					</div>
				</div>
				<!-- 関連リンク -->
				<div class="p-nav__block">
					<p class="p-nav__title">関連リンク</p>
					<ul class="p-nav__menu">
						<li class="p-nav__item"><a href="https://japanpt.org" target="_blank" rel="noopener">かえる勤怠管理</a></li>
						<li class="p-nav__item"><a href="https://itzmkt.com/company/" target="_blank" rel="noopener">会社概要</a></li>
						<!--<li class="p-nav__item"><a href="#" target="_blank" rel="noopener">CAERU AIガイド</a></li>-->
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>

</html>
