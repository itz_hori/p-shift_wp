<?php
	/**
	 * 検索フォームテンプレート
	 *
	 * @package    WordPress
	 */

?>

<form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search" class="p-search">
<?php
$class_name = get_query_var( 'class' );
?>
	<input type="search" name="s" id="s" placeholder="キーワードを入力して検索" class="c-input p-search__field">
	<button type="submit" class="c-button p-search__submit">
		<svg>
			<use xlink:href="#svg-icon-search"></use>
		</svg>
	</button>
</form>
