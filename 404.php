<?php
/**
 * 404エラーページのテンプレート
 *
 * @package    WordPress
 */

?>

<?php get_header(); ?>

<!-- section -->
<section class="l-section">
	<div class="l-container">
		<div class="c-grid">

			<!-- メイン -->
			<div class="c-grid__item -main">
				<div class="p-panel -large">
					<h2 class="c-heading -primary -icon">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icon-404.svg" alt="" width="32" height="28"><span>404 Not Found</span>
					</h2>
					<div class="p-panel__body">
						<p class="u-mt-24">
							ページが移動または削除されたか、URLの入力間違いの可能性があります。<br>
							申し訳ございませんが、ホームへ戻るか、お探ししたい情報をご検索ください。
						</p>
						<div class="p-error-search">
							<div class="p-error-search__button">
								<a class="c-button -primary -contact" href="<?php echo esc_url( home_url( '/' ) ); ?>">
									<svg class="u-svg-home">
										<use xlink:href="#svg-icon-home"></use>
									</svg><span>ホームへ戻る</span>
								</a>
							</div>
							<div class="p-error-search__field">
							<?php get_search_form(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- サイドバー -->
			<?php get_sidebar(); ?>

		</div><!-- ./c-grid -->
	</div><!-- .l-container -->
</section>

<?php get_footer(); ?>
