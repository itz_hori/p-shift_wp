<?php
/**
 * トップページテンプレート
 *
 * @package    WordPress
 */

?>

<?php get_header(); ?>

<!-- section -->
<section class="l-section">
	<div class="l-container">

		<!-- 各カテゴリパネル -->
		<section class="l-section__home-block">
			<div class="c-grid -gutter">
				<?php
				$args = array( 'hide_empty' => 0 ); /** 記事数0のカテゴリも取得 */

				$categories = get_categories( $args );
				foreach ( $categories as $category ) :
					?>

				<div class="c-grid__item -half">
					<div class="p-panel -home js-home-panel">
						<h2 class="c-heading -primary -icon p-panel__title">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icon-<?php echo esc_html( $category->slug ); ?>.svg"
								alt="">
							<span><?php echo esc_html( $category->cat_name ); ?></span>
						</h2>
						<div class="p-panel__body -mg-small">
							<ul class="p-panel__list">
							<?php
							$args = array(
								'post_type'      => 'post',
								'category_name'  => $category->slug, // 各カテゴリのslug名を指定!
								'posts_per_page' => 5, // MAX5件ずつ!
								'no_found_rows'  => true, // ページャーを使う時はfalseに!
							);

							$the_query = new WP_Query( $args );
							if ( $the_query->have_posts() ) :
								while ( $the_query->have_posts() ) :
									$the_query->the_post();
									?>

									<?php get_template_part( 'template-parts/panel-item' ); ?>

									<?php
									endwhile;
							endif;
							?>
							</ul>
						</div>
						<div class="p-panel__more">
							<a class="c-link-blue" href="<?php echo esc_url( home_url( $category->slug ) ); ?>/">
								<span>一覧を見る</span>
								<svg class="u-svg-arrow">
									<use xlink:href="#svg-icon-arrow"></use>
								</svg>
							</a>
						</div>
					</div>
				</div>

					<?php endforeach; ?>

			</div><!-- /.c-grid -->
		</section>

		<!-- お知らせ・サイドバー -->
		<section class="l-section__home-block">
			<div class="c-grid">

				<!-- メイン -->
				<div class="c-grid__item -main">
					<div class="p-panel -home">
						<h2 class="c-heading -primary -icon p-panel__title">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icon-info.svg" alt=""
								width="32" height="32"><span>お知らせ</span>
						</h2>
						<div class="p-panel__body">
							<ul class="p-panel__list">
							<?php
							$args = array(
								'post_type'     => 'info',
								'post_per-page' => 5,
							);

							$info_query = new WP_Query( $args );
							if ( $info_query->have_posts() ) :
								while ( $info_query->have_posts() ) :
									$info_query->the_post();
									?>

									<?php
										set_query_var( 'location', 'info' );
										get_template_part( 'template-parts/panel-item' );
									?>

									<?php
									endwhile;
							endif;
							?>
							</ul>
						</div>
						<div class="p-panel__more">
							<a class="c-link-blue" href="<?php echo esc_url( home_url( 'info' ) ); ?>/">
								<span>一覧を見る</span>
								<svg class="u-svg-arrow">
									<use xlink:href="#svg-icon-arrow"></use>
								</svg>
							</a>
						</div>
					</div>
				</div>

				<?php get_sidebar(); ?>

			</div><!-- /.c-grid -->
		</section>

	</div><!-- .l-container -->
</section>

<?php get_footer(); ?>
