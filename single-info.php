<?php
/**
 * お知らせ記事テンプレート
 * 「お知らせ」はカスタム投稿として管理
 *
 * @package    WordPress
 */

?>

<?php get_header(); ?>

	<div class="l-section">
		<div class="l-container">
			<div class="c-grid">

				<!-- メイン -->
				<div class="c-grid__item -main">
					<!-- 記事開始 -->
					<article class="p-article">
						<div class="p-panel -large">
							<h2 class="c-heading -primary -icon">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icon-info.svg" alt="" width="32" height="32"><span><?php the_title(); ?></span>
							</h2>
							<!-- 投稿・更新日 -->
							<?php get_template_part( 'template-parts/article-date' ); ?>
							<!-- 記事本文 -->
							<div class="p-article__body">
								<?php
								if ( have_posts() ) :
									while ( have_posts() ) :
										the_post();
										the_content();
										endwhile;
									endif;
								?>
							</div><!-- /.p-article__body -->

							<!-- 関連記事 -->
							<div class="p-article__related p-related">
								<h2 class="p-related__title">その他のお知らせ記事</h2>
								<ul class="p-related__list">
								<?php
								$args = array(
									'post_type'      => 'info',
									'posts_per_page' => -1,
									'post__not_in'   => array( $post->ID ), /* 表示中の個別記事を除外する */
								);

								$info_query = new WP_Query( $args );
								if ( $info_query->have_posts() ) :
									while ( $info_query->have_posts() ) :
										$info_query->the_post();
										?>

									<li><a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a></li>

										<?php
									endwhile;
								endif;
								?>
								</ul>
							</div><!-- /.p-article__related -->

						</div><!-- /.p-panel -->
					</article>
				</div>

				<!-- サイドバー -->
				<?php get_sidebar(); ?>

			</div>
			<!--/.c-grid -->

		</div><!-- .l-container -->
	</div>

<?php get_footer(); ?>
