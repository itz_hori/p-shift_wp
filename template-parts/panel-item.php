<?php
/**
 * 記事アイテムテンプレート
 *
 * ループ処理の中で使用（front-page.php / category.php ）
 *
 * @package    WordPress
 */

?>

<li class="p-panel__item">
	<a class="c-link-arrow" href="<?php the_permalink(); ?>">
	<?php
	/* get_query_var で呼び出す場所が「お知らせ」だった場合、日付のHTMLを追加 */
	$location = get_query_var( 'location' );
	if ( 'info' === $location ) {
		echo '<time>' . esc_html( get_the_time( 'Y年m月d日' ) ) . '</time>';
	}
	?>
		<span><?php the_title(); ?></span>
		<svg class="u-svg-arrow">
			<use xlink:href="#svg-icon-arrow"></use>
		</svg>
	</a>
</li>
