<?php
/**
 * 投稿日 + 更新日を出力する
 *
 * シングルページで使用（single.php / single-info.php ）
 *
 * @package    WordPress
 */

?>

<div class="p-article__date">
	<time>投稿日：<?php the_time( 'Y年m月d日' ); ?></time>
	<?php if ( get_the_time( 'Y年m月d日' ) !== get_the_modified_date( 'Y年m月d日' ) ) : ?>
		<time>最終更新日：<?php the_modified_date( 'Y年m月d日' ); ?></time>
	<?php endif; ?>
</div>
