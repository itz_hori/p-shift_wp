<?php
/**
 * タグ表示テンプレート
 *
 * ループ処理の中で使用（header.php / footer.php ）
 *
 * @package    WordPress
 */

$tag_list = get_tags( $args ); /** タグ一覧を取得 */

echo '<ul class="c-keyword">';
foreach ( $tag_list as $tag_item ) {
	echo '<li class="c-keyword__item"><a href="' . esc_url( get_tag_link( $tag_item->term_id ) ) . '/">' . esc_html( $tag_item->name ) . '</a></li>';
}
echo '</ul>';
