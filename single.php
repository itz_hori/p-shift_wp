<?php
/**
 * 個別投稿記事テンプレート
 *
 * @package    WordPress
 */

?>

<?php get_header(); ?>

	<div class="l-section">
		<div class="l-container">
			<div class="c-grid">

				<!-- メイン -->
				<div class="c-grid__item -main">
					<!-- 記事開始 -->
					<article class="p-article">
						<div class="p-panel -large">
							<!-- 投稿・更新日 -->
							<?php get_template_part( 'template-parts/article-date' ); ?>
							<!-- 記事本文 -->
							<div class="p-article__body">
								<?php
								if ( have_posts() ) :
									while ( have_posts() ) :
										the_post();

										the_content();
										endwhile;
									endif;
								?>
							</div><!-- /.p-article__body -->

							<!-- ページネーション -->
							<div class="p-article__pagination p-pagination">
							<?php
							/* 引数に true を指定で同じカテゴリの投稿を取得 */
							$next_post = get_previous_post( true );

							if ( ! empty( $next_post ) ) :
								$next_url   = get_permalink( $next_post->ID );
								$next_title = $next_post->post_title;
								?>
								<!-- 次の記事 -->
								<div class="p-pagination__item -next">
									<p class="p-pagination__title">次の記事へ</p>
									<div class="p-pagination__button">
										<a class="p-pagination__link" href="<?php echo esc_url( $next_url ); ?>">
											<span><?php echo esc_html( $next_title ); ?></span><svg class="u-svg-arrow">
												<use xlink:href="#svg-icon-arrow"></use>
											</svg>
										</a>
									</div>
								</div><!-- /.p-pagination__item -->

							<?php endif; ?>

							<?php
							/* 引数に true を指定で同じカテゴリの投稿を取得 */
							$prev_post = get_next_post( true );

							if ( ! empty( $prev_post ) ) :
								$prev_url   = get_permalink( $prev_post->ID );
								$prev_title = $prev_post->post_title;
								?>
								<!-- 前の記事 -->
								<div class="p-pagination__item -prev">
									<p class="p-pagination__title">前の記事へ</p>
									<div class="p-pagination__button">
										<a class="p-pagination__link" href="<?php echo esc_url( $prev_url ); ?>">
											<span><?php echo esc_html( $prev_title ); ?></span><svg class="u-svg-arrow -left">
												<use xlink:href="#svg-icon-arrow"></use>
											</svg>
										</a>
									</div>
								</div><!-- /.p-pagination__item -->
							<?php endif; ?>
							</div><!-- /.p-article__pagination -->

							<!--関連記事-->
							<div class="p-article__related p-related">
								<h2 class="p-related__title">このカテゴリの記事</h2>
								<ul class="p-related__list">
								<?php
								$current_url = get_the_permalink(); /* 現在のURL */
								$categories  = get_the_category();
								$category    = $categories[0];
								$args        = array(
									'post_type'      => 'post',
									'posts_per_page' => -1,
									'category__in'   => $category->cat_ID,
								);

								$the_query = new WP_Query( $args );
								if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) :
										$the_query->the_post();
										$anchor_url = get_the_permalink(); /* 各aタグリンク先URLを取得 */

										/* 現在のページURLとaタグリンク先URLを比較して、HTMLを結果でHTMLを出力 */
										if ( $current_url === $anchor_url ) {
											echo '<li>' . esc_html( get_the_title() ) . '</li>';
										} else {
											echo '<li><a href="' . esc_html( get_the_permalink() ) . '"><span>' . esc_html( get_the_title() ) . '</span></a></li>';
										}
										?>
										<?php
									endwhile;
								endif;
								?>
								</ul>
							</div><!-- /.p-article__related -->

						</div><!-- /.p-panel -->
					</article>
				</div>

				<!-- サイドバー -->
				<?php get_sidebar(); ?>

			</div>
			<!--/.c-grid -->

		</div><!-- .l-container -->
	</div>

<?php get_footer(); ?>
