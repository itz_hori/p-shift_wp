<?php
/**
 * Sidebarテンプレート
 *
 * @package    WordPress
 */

?>

<div class="c-grid__item -sub">

	<?php if ( ! is_front_page() ) : // TOPページでないとき! ?>
	<!-- メニュー -->
	<aside class="p-widget">
		<div class="p-panel -side-menu">
			<h2 class="c-heading -secondary -icon">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icon-book.svg" alt="" width="27" height="32"><span>カテゴリメニュー</span>
			</h2>
			<ul class="p-side-list">
				<?php
				$categories = get_categories();
				foreach ( $categories as $category ) : /** 各カテゴリーをループ */
					?>

				<li>
					<button class="p-side-list__button js-side-accordion-trigger">
						<span><?php echo esc_html( $category->cat_name ); ?></span>
						<svg class="u-svg-arrow -bottom">
							<use xlink:href="#svg-icon-arrow"></use>
						</svg>
					</button>
					<div class="p-side-list__accordion js-side-accordion">
						<ul>

						<?php
						$args      = array(
							'post_type'      => 'post',
							'category_name'  => $category->slug, // 各カテゴリのslug名を指定!
							'posts_per_page' => -1, // すべての件数!
							'no_found_rows'  => true, // ページャーを使う時はfalseに!
						);
						$the_query = new WP_Query( $args );
						if ( $the_query->have_posts() ) :
							while ( $the_query->have_posts() ) : /** 記事の数だけループ */
								$the_query->the_post();
								?>

							<li>
								<a class="c-link-arrow" href="<?php the_permalink(); ?>">
									<span><?php the_title(); ?></span>
									<svg class="u-svg-arrow">
										<use xlink:href="#svg-icon-arrow"></use>
									</svg>
								</a>
							</li>

								<?php
							endwhile;
						endif;
						?>
							<!-- 各カテゴリトップへのリンク -->
							<li>
								<a class="c-link-blue" href="<?php echo esc_url( home_url( $category->slug ) ); ?>/"><?php echo esc_html( $category->cat_name ); ?>トップ</a>
							</li>
						</ul>
					</div>
				</li>

				<?php endforeach; ?>

				<li>
				<?php
				$current_url = get_pagenum_link( get_query_var( 'paged' ) ); /* 現在のURLを取得 */
				$url_array   = explode( '/', $current_url ); /* 「/」でURLを分割し配列にする */
				array_splice( $url_array, -2, 2 ); /* 配列の末尾から要素を2つ削除 */
				$one_level_url = implode( '/', $url_array ); /* 「/」で配列をつなげて一階層上のURLとする */


				/* クソコード応急処置 */
				if ( is_tag() ) {
					$one_level_url = get_home_url();
				}

				if ( is_single() ) {
					$back_text = 'カテゴリトップへ戻る';
				} else {
					$back_text = 'トップへ戻る';
				}
				?>
					<a class="p-side-list__button -back c-link-blue" href="<?php echo esc_url( $one_level_url ); ?>">
						<svg class="u-svg-arrow -left">
							<use xlink:href="#svg-icon-arrow"></use>
						</svg><span><?php echo esc_html( $back_text ); ?></span>
					</a>
				</li>
			</ul><!-- /.p-side-list -->
		</div>
	</aside>
	<?php endif; ?>

	<!-- バナー -->
	<aside class="p-widget">
		<div class="p-banner">
			<figure class="p-banner__image">
				<a href="https://japanpt.org" target="_blank" rel="noopener"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/banner.png" alt="かえる勤怠管理"></a>
			</figure>
		</div>
	</aside>

	<!-- お問い合わせ -->
	<aside class="p-widget">
		<div class="p-panel -small">
			<h2 class="c-heading -secondary">お問い合わせ</h2>
			<div class="p-panel__module">
				<p class="p-panel__tel"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icon-phone.svg" alt="" width="24" height="24"><span>082-258-2988</span></p>
				<p class="p-panel__desc">（受付時間：平日 9:00～17:00）</p>
			</div>
			<div class="p-panel__button">
				<a class="c-button -primary" href="https://japanpt.org/contact/" target="_blank" rel="noopener">
					<svg class="u-svg-mail">
						<use xlink:href="#svg-icon-mail"></use>
					</svg>
					<span>メールでのお問い合わせ</span>
				</a>
			</div>
		</div>
	</aside>

	<!-- かえるAIガイドへのリンク -->
	<!--<aside class="p-widget">
		<div class="p-panel -small">
			<h2 class="c-heading -secondary">CAERU AIをご利用のお客様</h2>
			<div class="p-panel__module">
				<figure class="p-panel__image">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/logo-caeruai.svg" alt="CAERU AIガイド">
				</figure>
			</div>
			<div class="p-panel__button">
				<a class="c-button -primary -guide" href="#" target="_blank" rel="noopener">
					<svg class="u-svg-blank">
						<use xlink:href="#svg-icon-blank"></use>
					</svg>
					<span>CAERU AIガイドはこちら</span>
				</a>
			</div>
		</div>
	</aside>-->

</div>
